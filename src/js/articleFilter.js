'use strict'

// Permet de filtrer la selection en ajoutant ou supprimant la classe show

filterSelection("all")

function filterSelection(area) {
  let filter = document.getElementsByClassName("filterDiv");

  if (area == "all") {
    area = "";
  }

  for (let i = 0; i < filter.length; i++) {
    removeClass(filter[i], "show");

    if (filter[i].className.indexOf(area) > -1) {
      addClass(filter[i], "show");
    }
  }
}

// Ajoute la class "show"

function addClass(element, name) {
  let arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");

  for (let i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

// Supprime la class "show"

function removeClass(element, name) {
  let arr1, arr2;

  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (let i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// Ajoute la classe active au bouton courant (design différent)

let filterBtn = document.getElementById("filterBtn");
let buttons = filterBtn.getElementsByClassName("btn");

for (let i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener("click", function () {
    let current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}