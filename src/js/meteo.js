'use strict'

// Objet pour transformer les icones pour le CDN de Weather Icons
const weatherIcons = {
    "Rain": "wi wi-day-rain",
    "Clouds": "wi wi-day-cloudy",
    "Clear": "wi wi-day-sunny",
    "Snow": "wi wi-day-snow",
    "Mist": "wi wi-day-fog",
    "Drizzle": "wi wi-day-sleet",
}

// Récupère la latitude et longitude d'une ville avec l'API Nominatim et la météo du lieux de la randonnée avec l'API OpenWeatherMap

async function getLocalistation() {
    const city = document.getElementById('city').innerHTML

    const localisation = await window.fetch('https://nominatim.openstreetmap.org/search?city=' + city + '&countrycodes=fr&format=json&addressdetails=1')
        .then(response => response.json())
        .then(json => json[0])

    const lat = localisation.lat
    const lon = localisation.lon

    const meteo = await window.fetch('https://api.openweathermap.org/data/2.5/onecall?lat=' + lat + '&lon=' + lon + '&exclude=current,minutely,hourly&lang=fr&units=metric&appid=100e5f441b5f507fef5ac3ed28419f44')
        .then(response => response.json())
        .then(json => json)

    // Récupère les infos météo pour l'affichage
    displayMeteo(meteo)
}

getLocalistation()


// Extrait les diférents éléments du JSON pour avoir la météo sur 3 jours et ensuite les affiches.

function displayMeteo(data) {

    const condition = [
        data.daily[0].weather[0].main,
        data.daily[1].weather[0].main,
        data.daily[2].weather[0].main
    ]

    const description = [
        data.daily[0].weather[0].description,
        data.daily[1].weather[0].description,
        data.daily[2].weather[0].description
    ]

    const temperature = [
        data.daily[0].temp.min,
        data.daily[0].temp.max,
        data.daily[1].temp.min,
        data.daily[1].temp.max,
        data.daily[2].temp.min,
        data.daily[2].temp.max
    ]
    
    // Background Image suivant la condition
    document.querySelector('#weather-j0').className = condition[0]
    document.querySelector('#weather-j1').className = condition[1]
    document.querySelector('#weather-j2').className = condition[2]

    // Icone du temp pour les 3 jours
    document.querySelector('i.j0.wi').className = weatherIcons[condition[0]]
    document.querySelector('i.j1.wi').className = weatherIcons[condition[1]]
    document.querySelector('i.j2.wi').className = weatherIcons[condition[2]]

    // Description du temps pour les 3 jours
    document.querySelector('.condition.j0').textContent = strUcFirst(description[0])
    document.querySelector('.condition.j1').textContent = strUcFirst(description[1])
    document.querySelector('.condition.j2').textContent = strUcFirst(description[2])
    
    // Température min et max pour les 3 jours
    document.querySelector('.min-j0').textContent = Math.round(temperature[0])
    document.querySelector('.max-j0').textContent = Math.round(temperature[1])
    document.querySelector('.min-j1').textContent = Math.round(temperature[2])
    document.querySelector('.max-j1').textContent = Math.round(temperature[3])
    document.querySelector('.min-j2').textContent = Math.round(temperature[4])
    document.querySelector('.max-j2').textContent = Math.round(temperature[5])

    
}

// Met la première lettre de la chaine de caractère en majuscule
function strUcFirst(a){
    return (a+'').charAt(0).toUpperCase()+a.substr(1);
}





