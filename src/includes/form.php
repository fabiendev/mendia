<?php

// Récupère l'ID dans l'url s'il existe

function getId() {
	if (!empty($_GET['id'])) {
        return $_GET['id'];
    }
}

// Récupérer la value de $_GET pour les inputs des champs de recherches

function searchValue() {
    if (!empty($_GET['search'])) {
        return $_GET['search'];
    }
}


// Vérifie si un email est valide 

function validateEmail(string $mail, $fieldName = false) {
	if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) :
		return 'Merci de renseigner un email avec un format valide.';
	endif;
}


// Vérifie si un mot de passe est valide

function validatePassword($password) {
	if (!preg_match('/((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,50})/', $password)) {
		return false;
	}
}

// Vérifie si c'est un nombre entier

function validateInt(string $field, $fieldName = false) {
	if (!filter_var($field, FILTER_VALIDATE_INT)) :
		return 'Merci de renseigner un nombre entier.';
	endif;
}


// Vérifie si le pseudo existe déjà en BDD

function existUsername($username) {
    global $db;
    
    $sql = 'SELECT * FROM users 
    WHERE username = ?';
    $request = $db->prepare($sql);
    $request->execute([$username]);
    $result = $request->fetch();

    if (!empty($result)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

// Vérifie si l'email existe déjà en BDD

function existEmail($email) {
    global $db;
    
    $sql = 'SELECT * FROM users 
    WHERE email = ?';
    $request = $db->prepare($sql);
    $request->execute([$email]);
    $result = $request->fetch();

    if (!empty($result)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}