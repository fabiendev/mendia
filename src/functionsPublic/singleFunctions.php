<?php 
require_once('../includes/_dispacher.php');

// Fonction pour récupérer le contenu d'une randonnée en se basant sur l'id dans l'url

function getSingleHike() {
    global $db;

    if (isset($_GET['id']) && $_GET['id'] != "" && (int)$_GET['id']) {
        $data['id'] = $_GET['id'];
        $sql = 'SELECT hikes.id, title, introduction, description, information, area, city, elevation, duration, distance, hikes.picture, name, color, username
        FROM users
        JOIN hikes
        ON users.id = hikes.id_user
        JOIN levels
        ON hikes.id_level = levels.id
        WHERE hikes.id = :id';
        $request = $db->prepare($sql);
        $request->execute($data);
        $singleHike = $request->fetchAll();
        
        return $singleHike;

    } else {
        header('Location: ' . '404.php' );
    }
}

$singleHike = getSingleHike();



// Fonction pour récupérer les commentaires d'une randonnée
    
function getComments() {
    global $db;
    
    $data['id'] = (int)$_GET["id"];
    $sql = 'SELECT comments.*, users.username, users.picture
    FROM comments 
    JOIN users 
    ON comments.id_user = users.id 
    AND comments.id_hike = :id
    ORDER BY posted DESC';
    $request = $db->prepare($sql);
    $request->execute($data);
    $comments = $request->fetchAll();

    return $comments;
}

$comments = getComments();

// Fonction pour afficher le nombre de commentaires d'une randonnée

function nbComments() {
    global $db;
    
    $data['id'] = (int)$_GET["id"];
    $sql = 'SELECT COUNT(*) FROM comments 
    WHERE id_hike = :id';
    $request = $db->prepare($sql);
    $request->execute($data);
    $nbComment = $request->fetch();

    return $nbComment;
}

$nbComments = nbComments();

// Fonction pour poster un commentaire

function addComment() {
    if(isset($_SESSION["id"])) {
        global $db;
    
        $error = "";

        extract($_POST);

        if(!empty($comment)) {

            $data = [
                "id_user" => $_SESSION["id"],
                "id_hike" => $_GET["id"],
                "comment" => $_POST['comment']
            ];
            $sql = 'INSERT INTO comments (id_user, id_hike, comment) 
            VALUES (:id_user, :id_hike, :comment)';
            $request = $db->prepare($sql);
            $request->execute($data);

            unset($_POST);

            header('Location: ' . 'single.php?id=' . $_GET["id"] . '#comment' );

        } else {
            $error .= "Vous devez saisir un commentaire";
            return $error;
        }      
    }
}

$error = addComment();

// Génère le texte de la notif dans $_SESSION pour les erreurs dans le formulaire des commentaires

function commentNotif($error) {
    if(isset($error)) {
        $_SESSION['notif'] = '<div class="error_notif">' . $error .'</div>';
    } 
}

$errorNotif = commentNotif($error);

// Affiche la notification d'erreur pour le formulaire des commentaires

function showNotif() {
    if (!empty($_SESSION['notif'])) {
		$notif = $_SESSION['notif'];

		unset($_SESSION['notif']);

		return $notif;
    }
}

$notif = showNotif();
