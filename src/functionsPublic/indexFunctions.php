<?php

require_once('../includes/_dispacher.php');


function getAllCityHikes() {
    global $db;

    $sql = "SELECT id, title, city, picture 
    FROM hikes";
    $request = $db->query($sql);
    $result = $request->fetchAll();

    return $result;
}

$jsonHike = getAllCityHikes();
$hikeJson = json_encode($jsonHike, JSON_UNESCAPED_UNICODE);
