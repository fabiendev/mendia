<?php
require_once('../includes/_dispacher.php');

// Fonction pour se déconnecter puis vider et détruire la session

if (isset($_SESSION['auth']) && isset($_SESSION['role'])) {
	unset($_SESSION['auth']);
	unset($_SESSION['role']);
	session_destroy();
}

header('Location: ' . 'login.php');
die();