<?php
require_once('../functionsAdmin/admin_indexFunctions.php');
get_header('admin');
?>

<section class="container-fluid mt-3 mb-4">
    <div class="container mb-3 g-0">
        <div class="row justify-content-between g-0">
            <div class="col-6">
                <a href="../pages-public/index.php" class="btn primary-color text-white fw-bold" title="Accueil">Retour sur le site</a>
            </div>
            <div class="col-6 text-end">
                <a href="../pages-public/logout.php" class="btn btn-danger fw-bold" title="Déconnexion">Se déconnecter</a>
            </div>
        </div>
    </div>
    <div class="container bg-light rounded-3 p-5 mb-3 g-0">
        <h1 class="text-center fw-bold mb-5">Tableau de bord</h1>
        <h3 class="text-center mb-2">Vous pouvez consulter sur cette page:</h3><br> 
        <div class="row justify-content-around g-0">
            <div class="col-md-3 col-lg-4 mt-3 text-center">
                <a href="#user_info" class="btn btn-lg primary-color text-white fw-bold" title="Informations">Mes informations</a>
            </div>
            <div class="col-md-3 col-lg-4 mt-3 text-center">
                <a href="#user_hike" class="btn btn-lg primary-color text-white fw-bold" title="Randonnées">Mes randonnées</a>
            </div>
            <div class="col-md-3 col-lg-4 mt-3 text-center">
                <a href="#user_comment" class="btn btn-lg primary-color text-white fw-bold" title="Commentaires">Mes commentaires</a>
            </div>
        </div>
    </div>
    <div class="container g-0">
        <div class="row g-0">
            <div id="user_info" class="col-12 mt-4 g-0 border">
                <div class="container p-3">
                    <div class="row g-0">
                        <div class="col-6">
                            <h4 class="fw-bold">Mes informations</h4>
                            <h6 class="fst-italic">Les informations de votre compte</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a href="admin_edit-user.php?id=<?php echo $_SESSION['id']; ?>" class="btn btn-warning" title="Modifier">Modifier</a>	
                        </div>
                    </div>
                </div>
                <div class="container mb-2">
                    <div class="row p-3 g-0">
                        <?php foreach ($userInfo as $value) { ?>
                            <div class="col-md-6 p-3 bg-light border text-center">
                                <?php if (!empty($value['picture'])) { ?>
                                    <img src="../assets/image/user/<?php echo $value['picture']; ?>" class="card-img-top user-avatar" alt="avatar">
                                <?php } else { ?>
                                    <img src="../assets/image/user/default.png" class="card-img-top user-avatar" alt="avatar">
                                <?php } ?>
                                <h5 class="card-title fw-bold mt-3">Avatar</h5>
                            </div>
                            <div class="col-md-6 g-0">
                                <div class="row h-100 g-0">
                                    <div class="col-12 p-3 bg-light border h-33 text-center">
                                        <div class="mt-4">
                                            <h5 class="fw-bold">Pseudo</h5>
                                            <p class="mt-3"><?php echo $value['username']; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-12 p-3 bg-light border h-33 text-center">
                                        <div class="mt-4">
                                            <h5 class="fw-bold">Email</h5>
                                            <p class="mt-3"><?php echo $value['email']; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-12 p-3 bg-light border h-33 text-center">
                                        <div class="mt-4">
                                            <h5 class="fw-bold">Localisation</h5>
                                            <p class="mt-3"><?php echo $value['localisation']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div id="user_hike" class="col-12 mt-4 g-0 border">
                <div class="container p-3">
                    <div class="row g-0">
                        <div class="col-6">
                            <h4 class="fw-bold">Mes randonnées</h4>
                            <h6 class="fst-italic">Les randonnées que vous avez posté</h6>
                        </div>
                        <div class="col-6 text-end">
                            <a href="admin_add-hike.php" class="btn btn-success" title="Ajouter">Ajouter</a>	
                        </div>
                    </div>
                </div>
                <div class="container p-3">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="primary-color text-white text-center fw-bold">
                                <tr class="d-flex">
                                    <th class="col">Titre</th>
                                    <th class="col">Ville</th>
                                    <th class="col">Posté</th>
                                    <th class="col-5 col-md-4">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php foreach ($userHike as $value) { ?>
                                    <tr class="d-flex">
                                        <td class="col align-middle"><?php echo $value['title']; ?></td>
                                        <td class="col align-middle"><?php echo $value['city']; ?></td>
                                        <td class="col align-middle"><?php echo dateFormat($value['posted'], false); ?></td>
                                        <td class="col-5 col-md-4 align-middle">
                                            <div class="d-flex flex-column flex-md-row justify-content-md-center">
                                                <a href="../pages-public/single.php?id=<?php echo $value['id']; ?>" class="btn btn-primary btn-sm me-md-1" title="Voir">Voir</a>
                                                <a href="admin_edit-hike.php?id=<?php echo $value['id']; ?>" class="btn btn-warning btn-sm me-md-1 mt-1 mt-md-0" title="Modifier">Modifier</a>					
                                                <a href="admin_delete-hike.php?id=<?php echo $value['id']; ?>" class="btn btn-danger btn-sm mt-1 mt-md-0" title="Supprimer">Supprimer</a>
                                            </div>					
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="user_comment" class="col-12 mt-4 g-0 border">
                <div class="container p-3">
                    <h4 class="fw-bold">Mes commentaires</h4>
                    <h6 class="fst-italic">Les commentaires que vous avez posté sur les randonnées</h6>
                </div>
                <div class="container p-3">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="primary-color text-white text-center fw-bold">
                                <tr class="d-flex">
                                    <th class="col">Commentaire</th>
                                    <th class="col">Posté</th>
                                    <th class="col">Randonnée</th>
                                    <th class="col-5 col-md-4">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php foreach ($userComment as $value) { ?>
                                    <tr class="d-flex">
                                        <td class="col align-middle"><?php echo substr($value['comment'], 0, 30) . '...'; ?></td>
                                        <td class="col align-middle"><?php echo dateFormat($value['posted'], false); ?></td>
                                        <td class="col align-middle"><?php echo getHikeName($value['id_hike'])['title']; ?></td>
                                        <td class="col-5 col-md-4 align-middle">
                                            <div class="d-flex flex-column flex-md-row justify-content-md-center">
                                                <a href="../pages-public/single.php?id=<?php echo $value['id_hike']; ?>#comment" class="btn btn-primary btn-sm me-md-1" title="Voir">Voir</a>					
                                                <a href="admin_delete-comment.php?id=<?php echo $value['id']; ?>" class="btn btn-danger btn-sm mt-1 mt-md-0" title="Supprimer">Supprimer</a>
                                            </div>				
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer('admin'); ?>