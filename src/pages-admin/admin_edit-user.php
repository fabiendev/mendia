<?php
require_once('../functionsAdmin/admin_editUserFunctions.php');
get_header('admin');
?>

<section class="container p-3 mb-3">
	<div class="row mb-4 g-0">
		<h1 class="col-6 fw-bold">Modifier un utilisateur</h1>
		<div class="col-6 text-end">
			<a href="admin_index.php" class="btn btn-primary" title="Dashboard">Retour</a>
		</div>
	</div>
	<div class="container g-0">
		<div class="container">
			<?php if(!empty($_POST)) {
				$erreur = updateUser(); 
				if(isset($erreur)) {
					if($erreur) {
						foreach($erreur as $value) { ?>
						<div class="error_notif">
							<div class="alert alert-danger" role="alert"><?php echo $value; ?></div>
						</div>
						<?php } 
					} else { ?>
						<div class="confirmation_notif">
							<div class="alert alert-success" role="alert">L'utilisateur a bien été modifié</div>
						</div>
					<?php }
				} 
			} ?>
		</div>
		<form method="post" action="" id="send" enctype="multipart/form-data">
			<div class="form-group">
				<label for="username">Pseudo</label>
				<input type="text" class="form-control" name="username" id="username" value="<?php echo $userInfo['username'] ?>">
			</div>
			<div class="form-group mt-3">
			<label for="email">Email</label>
				<input type="email" class="form-control mt-2" name="email" id="email" value="<?php echo $userInfo['email'] ?>">
			</div>
			<div class="form-group mt-3">
				<label for="password">Mot de passe</label>
				<input type="password" class="form-control mt-2" aria-describedby="mdpHelp" name="password" id="password">
				<small id="mdpHelp" class="form-text text-muted">Doit être composé d'au moins: 1 minuscule, 1 majuscule, 1 chiffre, 1 caractère spécial, et doit avoir minimum 8 caractères.</small>
			</div>
			<div class="form-group mt-3">
				<label for="">Vérification du mot de passe</label>
				<input type="password" class="form-control mt-2" name="passwordConfirm" id="passwordConfirm">
			</div>
			<div class="form-group mt-3">
				<label for="localisation">Localisation</label>
				<input type="text" class="form-control mt-2" name="localisation" id="localisation" value="<?php echo $userInfo['localisation'] ?>">
			</div>
			<?php if ($_SESSION['role'] == 'admin') { ?>
				<div class="form-group mt-3">
				<label for="role">Rôle</label>
				<select class="form-control mt-2" aria-describedby="roleHelp" name="role" id="role">
					<option value="<?php echo $userInfo['role']; ?>"><?php echo $userInfo['role']; ?></option>
						<option value="user">Utilisateur</option>
						<option value="admin">Administrateur</option>
				</select>
				<small id="roleHelp" class="form-text text-muted">Par défaut le rôle est "user", vous pouvez choisir "admin" dans le menu.</small>
			</div>
			<?php } ?>
			<div class="form-group mt-3">
				<label for="picture">Image de profil</label>
				<input class="form-control-file mt-2" type="file" id="picture" name="picture">
			</div>
			<div>
				<input type="hidden" name="id" value="<?php echo getId(); ?>">
			</div>
			<div class="form-group mt-4">
				<button type="submit" class="btn btn-primary" id="submit">Valider</button>
			</div>
		</form>
	</div>
</section>

<?php get_footer('admin'); ?>