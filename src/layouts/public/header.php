<?php require_once('../includes/_dispacher.php'); ?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mendia - Parcourez les pyrénées</title>
<link rel="stylesheet" href="../../src/assets/css/main.css">
<script async src="https://kit.fontawesome.com/8c1aae2851.js" crossorigin="anonymous"></script>
<script async src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
<script async src="../../src/js/headerPublic.js"></script>
<script async src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
</head>
<body>
<header class="header">
    <div class="header__container flex-row flex-row--between">
        <div class="header__logo">
            <a class="header__logo-link" href="../../src/pages-public/index.php">
                <img src="../../src/assets/image/logo/mountain.png" alt="logo" class="header__logo-picture">
            </a>
        </div>
        <span class="header__toggle" id="js-navbar-toggle">
            <i class="header__toggle-icon fas fa-bars"></i>
        </span>
        <div class="header-desktop">
            <nav class="header-desktop__nav">
                <ul class="header-desktop__menu flex-row flex-row--between">
                    <li class="header-desktop__menu-item"><a class="header-desktop__menu-link" href="../../src/pages-public/guide.php">Guide</a></li>
                    <li class="header-desktop__menu-item"><a class="header-desktop__menu-link" href="../../src/pages-public/inspiration.php">Inspiration</a></li>
                    <li class="header-desktop__menu-item"><a class="header-desktop__menu-link" href="../../src/pages-public/safety.php">Sécurité</a></li>
                    <li class="header-desktop__menu-item header-desktop__menu-item--member"><a class="header-desktop__menu-link header-desktop__menu-link--member" href="../../src/pages-admin/admin_index.php" title="Mon Compte"><i class="fas fa-user-circle"></i></a></li>
                </ul> 
            </nav>
        </div>
    </div>
</header>
<div class="header-mobile" id="js-menu">
    <nav class="header-mobile__nav">
        <ul class="header-mobile__menu flex-column">
            <li class="header-mobile__menu-item"><a class="header-mobile__menu-link" href="../../src/pages-public/guide.php">Guide</a></li>
            <li class="header-mobile__menu-item"><a class="header-mobile__menu-link" href="../../src/pages-public/inspiration.php">Inspiration</a></li>
            <li class="header-mobile__menu-item"><a class="header-mobile__menu-link" href="../../src/pages-public/safety.php">Sécurité</a></li>
            <li class="header-mobile__menu-item"><a class="header-mobile__menu-link header-mobile__menu-link--member" href="../../src/pages-admin/admin_index.php" title="Mon Compte"><i class="fas fa-user-circle"></i></a></li>
        </ul> 
    </nav>
</div>
<main>