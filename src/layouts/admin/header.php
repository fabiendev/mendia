<?php require_once('../includes/_dispacher.php');
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Votre compte</title>
	<link rel="stylesheet" href="../../src/assets/css/admin.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-sm navbar-dark bg-dark p-3">
		<div class="container g-0">
			<a class="navbar-brand" href="admin_index.php" title="Tableau de bord">Tableau de bord</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<?php if ($_SESSION['role'] == 'admin') { ?>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" title="Menu Administrateur" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Administrateur
							</a>
							<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
								<li><a class="dropdown-item" href="admin_list-users.php" title="Utilisateurs">Tous les utilisateurs</a></li>
								<li><a class="dropdown-item" href="admin_list-hikes.php" title="Randonnées">Toutes les randonnées</a></li>
								<li><a class="dropdown-item" href="admin_list-comments.php" title="Commentaires">Tous les commentaires</a></li>
							</ul>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</nav>
	<main>
		<?= displayNotif(); ?>