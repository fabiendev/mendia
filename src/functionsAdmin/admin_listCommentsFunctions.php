<?php
require_once('../includes/_dispacher.php');

// Vérifie le rôle de l'utilisateur et le renvoi vers l'index s'il n'est pas admin
checkRole();

// Récupère toutes les randonnées de la BDD

function getComments() {
    global $db;

    $sql = 'SELECT id, id_user, id_hike, comment, posted
    FROM comments
    ORDER by posted';
    $request = $db->query($sql);
    $results = $request->fetchALL();

    return $results;
}

$allComments = getComments();
