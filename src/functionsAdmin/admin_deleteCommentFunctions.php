<?php
require_once('../includes/_dispacher.php');

// Récupère le commentaire courant via l'ID

function getCurrentComment() {
	global $db;
	$data['id'] = $_GET['id'];
	$sql = 'SELECT comment, id_user FROM comments WHERE id = :id';
	$request = $db->prepare($sql);
	$request->execute($data);
	$results = $request->fetch();

	return ($results) ? $results : [];
}

$currentComment = getCurrentComment();

// Vérifie si le commentaire existe en BDD

function existComment($currentComment) {
	if (empty($currentComment)) {
		header('Location: ' . 'admin_index.php'); 
		die();
    }
}

existComment($currentComment);

// Si l'utilisateur n'a pas le rôle 'admin' vérifie que le commentaire lui appartient

$currentUserRole = $_SESSION['role']; 

function isMyComment($currentComment, $currentUserRole) {
    if ($currentUserRole !== 'admin' && $_SESSION['id'] !== $currentComment['id_user']) {
        header('Location: ' . 'admin_index.php');
    }
}

isMyComment($currentComment, $currentUserRole);


// Fonction pour supprimer un commentaire après confirmation

function delete() {
	global $db;

	if (!empty($_GET['confirm'])) :
		$data['id'] = $_GET['id'];
		$sql = 'DELETE FROM comments WHERE id = :id';
		$request = $db->prepare($sql);
		$request->execute($data);

		notif('Le commentaire a bien été supprimé.', 'success');
	
		header('Location: ' . 'admin_index.php');
		die();
	endif;
}

delete();