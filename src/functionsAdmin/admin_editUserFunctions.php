<?php
require_once('../includes/_dispacher.php');

// Vérifie l'url et renvoie vers la page Liste utilisateur (ou admin_index si pas admin) si id est vide ou n'est pas créé (fonction sur la page tools.php)
validGetId();

// Vérifie si l'ID de $_GET correspond à un utilisateur dans la BDD et récupère les infos

function existUser() {
    $result = [];
	if (!empty($_GET['id'])) { 
		global $db;
		$data['id'] = $_GET['id'];
		$sql = 'SELECT * FROM users WHERE id = :id';
		$request = $db->prepare($sql);
		$request->execute($data);
		$result = $request->fetch();
		
		if (empty($result)) {
			header('Location: ' . 'admin_list-users.php');
			die();
        }
    }
	return $result;
}

$userInfo = existUser();

// Si l'utilisateur n'a pas le rôle 'admin' vérifie que le profil qu'il cherche à modifier lui appartient

$currentUserRole = $_SESSION['role']; 

function isMyProfile($userInfo, $currentUserRole) {
    if ($currentUserRole !== 'admin' && $_SESSION['id'] !== $userInfo['id']) {
        header('Location: ' . 'admin_index.php');
    }
}

isMyProfile($userInfo, $currentUserRole);

// Met à jour les infos de l'utilisateur en BDD

function updateUser() {
    global $db;

    extract($_POST);
    
    $validation = true;
    $erreur = [];
    
    if (empty($username) || empty($email)) {
        $validation = false;
        $erreur[] = 'Vous ne pouvez pas supprimer ces champs.';
    }
    
    if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $validation = false;
        $erreur[] = 'L\'adresse e-mail n\'est pas valide.';
    }

    if (validatePassword($password)) {
        $validation = false;
        $erreur[] = 'Merci de renseigner un mot de passe au bon format.';
    }

    if ($passwordConfirm != $password) {
        $validation = false;
        $erreur[] = 'Le mot de passe de confirmation est incorrect.';
    }

    if (!empty($FILES['picture']['type']) && $_FILES['picture']['type'] != 'image/jpg' && $_FILES['picture']['type'] != 'image/jpeg' && $_FILES['picture']['type'] != 'image/png') {
        $validation = false;
        $erreur[] = 'Merci de charger un fichier avec l\'une de ces extensions : jpg, jpeg, png';
    }

    if ($validation) {

        $data = [
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'localisation' => $_POST['localisation'],
            'id' => $_POST['id']
        ];

        $picture = '';
        if (!empty($_FILES['picture']['name'])) {
            move_uploaded_file($_FILES['picture']['tmp_name'], '../assets/image/user/' . basename($_FILES['picture']['name']));

            $data['picture'] = basename($_FILES['picture']['name']);
            $picture = ' picture = :picture, ';
        }
    
        $password = '';
        if (!empty($_POST['password'])) :
            $data['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $password = ' password = :password, ';
        endif;

        $role = '';
        if (!empty($_POST['role'])) :
            $data['role'] = $_POST['role'];
            $role = ' role = :role, ';
        endif;
    
        $sql = 'UPDATE users SET username = :username, email = :email, ' . $password . $picture . $role .' localisation = :localisation
        WHERE id = :id';
        $request = $db->prepare($sql);
        $request->execute($data);

    }
    return $erreur;
}
