<?php
require_once('../includes/_dispacher.php');

// Verifie le rôle de l'utilisateur et le renvoi vers l'index s'il n'est pas admin
checkRole();

// Ajouter un utilisateur en BDD

function addUser() {
    global $db;
    
    extract($_POST);
    
    $validation = true;
    $erreur = [];
    
    if (empty($username) || empty($email) || empty($password) || empty($passwordConfirm)) {
        $validation = false;
        $erreur[] = 'Tous les champs requis sont obligatoires.';
    }
    
    if (existUsername($username)) {
        $validation = false;
        $erreur[] = 'Ce pseudo est déjà pris.';
    }

    if (existEmail($email)) {
        $validation = false;
        $erreur[] = 'Cette adresse e-mail est déjà enregistrée.';
    }
    
    if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $validation = false;
        $erreur[] = 'L\'adresse e-mail n\'est pas valide.';
    }

    if (validatePassword($password)) {
        $validation = false;
        $erreur[] = 'Merci de renseigner un mot de passe au bon format.';
    }

    if ($passwordConfirm != $password) {
        $validation = false;
        $erreur[] = 'Le mot de passe de confirmation est incorrect.';
    }

    if (!empty($FILES['picture']['type']) && $_FILES['picture']['type'] != 'image/jpg' && $_FILES['picture']['type'] != 'image/jpeg' && $_FILES['picture']['type'] != 'image/png') {
        $validation = false;
        $erreur[] = 'Merci de charger un fichier avec l\'une de ces extensions : jpg, jpeg, png';
    }
    
    if ($validation) {

        move_uploaded_file($_FILES['picture']['tmp_name'], '../assets/image/user/' . basename($_FILES['picture']['name']));

        $data = [
			'username' => $username,
            'email' => $email,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'localisation' => $localisation,
            'picture' => basename($_FILES['picture']['name']),
			'role' => $role
        ];
        $sql = 'INSERT INTO users(username, email, password, localisation, picture, role) 
        VALUES(:username, :email, :password, :localisation, :picture, :role)';
        $request = $db->prepare($sql);
        $request->execute($data);
		
        unset($_POST);
    }
    
    return $erreur;
}



