<?php
require_once('../functionsPublic/registerFunctions.php');
get_header('public');
?>

<section class="register">
    <div class="register__container">
        <div class="register__head">
            <h1 class="register__head-title title title--medium">Créez un compte</h1>
        </div>
        <div class="register__main">
            <div class="register__required">
                <span class="register__required-description">* Champs requis</p>
            </div>
            <form method="post" action="" id="send" enctype="multipart/form-data" class="register__form">
                <div class="register__form-group">
                    <label for="username" class="sr-only">Pseudo</label>
                    <input type="text" name="username" id="username" placeholder="Pseudo*" value="<?php if(isset($_POST["username"])) echo $_POST["username"] ?>" class="register__form-input">
                </div>
                <div class="register__form-group">
                <label for="email" class="sr-only">Email</label>
                    <input type="email" name="email" id="email" placeholder="Email*" value="<?php if(isset($_POST["email"])) echo $_POST["email"] ?>" class="register__form-input">
                </div>
                <div class="register__form-group">
                    <label for="password" class="sr-only">Mot de passe</label>
                    <input type="password" name="password" id="password" placeholder="Mot de passe*" class="register__form-input">
                    <span class="register__form-description">Une minuscule, une majuscule, un chiffre, un caractère spécial, mini huit caractères.</span>
                </div>
                <div class="register__form-group">
                    <label for="" class="sr-only">Vérification du mot de passe</label>
                    <input type="password" name="passwordConfirm" id="passwordConfirm" placeholder="Vérification du mot de passe*" class="register__form-input">
                </div>
                <div class="register__form-group">
                    <label for="localisation" class="sr-only">Localisation</label>
                    <input type="text" name="localisation" id="localisation" placeholder="Localisation" value="<?php if(isset($_POST["localisation"])) echo $_POST["localisation"] ?>" class="register__form-input">
                </div>
                <div class="register__form-group">
                    <label for="picture" class="register__form-label">Avatar</label>
                    <input class="form-control-file" type="file" id="picture" name="picture">
                </div>
                <?php if(!empty($_POST)) {
                    $erreur = inscription();
                    if(isset($erreur)) {
                        if($erreur) {
                            foreach($erreur as $value) { ?>
                            <div class="register__form-notif error_notif">
                                <span class="error_message"><?= $value; ?></span>
                            </div>
                            <?php } 
                        } else { ?>
                            <div class="register__form-notif confirmation_notif">
                                <span class="confirmation_message">Votre inscription a bien été prise en compte !</span>
                            </div>
                        <?php }
                    }
                } ?>
                <div class="register__form-group">
                    <input type="submit" id="submit" value="M'inscrire" class="register__form-btn btn">
                </div>
            </form>
        </div>
        <div class="register__login">
            <h2 class="register__login-subtitle">Vous avez déjà un compte ?</h4>
            <a href="login.php" title="Login" class="register__login-link" >Connectez-vous</a>
        </div>
    </div>
</section>
</main>
</body>
</html>