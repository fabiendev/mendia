<?php 
require_once('../functionsPublic/safetyFunctions.php');
get_header('public'); 
?>

<section class="safety">
    <div class="safety__head">
        <h1 class="safety__head-title title title--big">Sécurité en montagne</h1>
        <span class="safety__head-subtitle title title--small"> Partir en sécurité, c'est rellement profiter de sa sortie !</span>
    </div>
    <div class="safety__content container flex-column">
        <div class="safety__introduction">
            <p>C'est page a pour but de rassembler des conseils utiles pour la sécurité en montagne. N'hésitez à la consultez avant vos sorties en montagne.
            Vous trouverez également des informations de sécurité sur <strong><a href="https://www.pompiers.fr/grand-public/prevention-des-risques/prevention-des-risques-en-montagne">le site des sapeurs-pompiers</a></strong> ou sur le site de la <strong><a href="https://www.ffrandonnee.fr/_396/securite-en-rando.aspx">Fédération Française de la Randonnée Pédestre</a></strong>.</p>
        </div>
        <div class="safety__item">
            <h2 class="safety__item-title title title--medium">Avant une sortie</h2>
            <div class="safety__item-component" x-data="{ open: false }">
                <span class="safety__item-btn btn btn--purple" @click="open = true">Lire les conseils</span>
                <div class="safety__item-dropdown" x-show="open" @click.away="open = false">
                    <p>Avant votre sortie en montagne, respectez toujours les conseils suivants afin de pratiquer votre activité en sécurité.</p>
                    <ul class="safety__item-list">
                        <li><strong>Choisissez une activité et un parcours adaptés</strong> à votre expérience et au niveau du moins entrainé du groupe.</li>
                        <li><strong>Prenez conseil auprès des professionnels</strong> (guides de haute montagne, accompagnateurs en moyenne montagne…), des offices de tourisme...</li>
                        <li><strong>Apprenez à reconnaitre le balisage</strong> que vous rencontrez sur les sites de pratiques (voir la section balisage plus bas)</li>
                        <li><strong>Consultez régulièrement</strong> <a href="http://www.meteofrance.com/accueil">les prévisions météo</a>  et prévoyez un itinéraire de remplacement ou n’hésitez pas à renoncer si les conditions s’annoncent défavorables.
                        <li><strong>Réservez votre place à l’avance</strong> si vous prévoyez de passer la nuit en refuge de montagne. Pensez à annuler en cas de changement.</li>
                        <li><strong>Informez une personne de votre programme</strong> et de l’heure probable de votre retour ainsi que de votre itinéraire. Evitez autant que possible de partir seul-e.</li>
                        <li><strong>Vérifiez visuellement l’usure</strong> de votre matériel.</li>
                        <li><strong>Vérifier que le lieu de votre sortie n'est pas à soumis à des écobuages</strong> (feux de broussaille de l'agriculture) en appelant la mairie ou l'office de tourisme ou en consultant les cartes en ligne. <strong><a href="https://jaimelagriculture64.fr/les-balades/organiser-sa-sortie-en-montagne-en-periode-decobuage/preparer-sa-sortie-en-montagne/">Exemple ici pour le 64.</a></strong></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="safety__item">
            <h2 class="safety__item-title title title--medium">Votre équipement</h2>
            <div class="safety__item-component" x-data="{ open: false }">
                <span class="safety__item-btn btn btn--purple" @click="open = true">Lire les conseils</span>
                <div class="safety__item-dropdown" x-show="open" @click.away="open = false">
                    <p>Avant votre sortie en montagne, attachez une importance particulière à la sécurité de votre équipement et suivez ces quelques conseils afin d’être certain-e de ne rien oublier !
                    <p><strong>Emportez avec vous :</strong></p>
                    <ul class="safety__item-list">
                        <li>De quoi vous <strong>alimenter et vous hydrater</strong>.</li>
                        <li><strong>Des vêtements adaptés à la météo</strong>, en sachant qu'elle peut changer rapidement : de quoi vous protéger du soleil (lunettes, casquette/chapeau, crème solaire), du vent (coupe-vent), du
                        froid (pull, pantalon, bonnet, gants) et de la pluie.</li>
                        <li>Equipez-vous avec de <strong>bonnes chaussures de randonnée</strong> adaptées à votre itinéraire.</li>
                        <li><strong>Un téléphone portable</strong> et les numéros d’urgence utiles (112).</li>
                        <li><strong>Une carte détaillée du lieu et un GPS</strong> (ou une boussole et un altimètre). Ne comptez pas uniquement sur votre portable : le réseau n’est pas toujours disponible en zone montagneuse.</li>
                        <li>Un couteau, un sifflet et une lampe de poche ou frontale.</li>
                        <li>Emportez <strong>une trousse de premiers secours</strong> (pansements compressifs, bandes, sparadrap…), et d’une couverture de survie.</li>
                        <li>La moyenne montagne enneigée <strong>s’apparente à un terrain d’alpinisme</strong>, soyez équipé-e en conséquence  : crampons, piolet, corde si besoin, afin de réduire votre risque de glissade sur les névés pentus.</li>
                    </ul>
                </div>
            </div>  
        </div>
        <div class="safety__item">
            <h2 class="safety__item-title title title--medium">Pendant une sortie</h2>
            <div class="safety__item-component" x-data="{ open: false }">
                <span class="safety__item-btn btn btn--purple" @click="open = true">Lire les conseils</span>
                <div class="safety__item-dropdown" x-show="open" @click.away="open = false">
                    <p>Une fois votre sortie en montagne bien préparée, respectez ces quelques conseils lors de la pratique de votre activité pour que la montagne reste un plaisir.</p>
                    <p><strong>Les bons réflexes:</strong></p>
                    <ul class="safety__item-list">
                        <li>Ne vous surestimez pas, dosez vos efforts.</li>
                        <li>Sachez renoncer, faire demi-tour ou raccourcir votre sortie si vous êtes fatigué-e, si les conditions de pratique se dégradent ou si un membre n’est plus en mesure de suivre le rythme.
                        <li>Restez toujours localisable : téléphone portable, réflecteur de type RECCO si vous partez en haute montagne, DVA (détecteur de victime d’avalanche)...
                        <li>Ne comptez pas uniquement sur votre portable : il n’est pas toujours possible de capter un réseau en zone montagneuse.</li>
                        <li>Portez particulièrement attention à vos enfants et vos chiens. Ils ont tendance à se dépenser sans compter, à se déshydrater et à s’épuiser rapidement. Soyez également vigilant-e avec les tout-petits lorsque vous les transportez sur le dos (risques de coups de soleil, déshydratation, refroidissement, etc...). Renseignez-vous si l'endroit est un parc national car les chiens n'y sont pas autorisés. Apporter de l'eau pour vos chiens.</li>
                        <li>Gardez vos distances avec les troupeaux en alpages et les chiens de protection (Patou). Restez calme à leur égard.</li>
                        <li>En période de canicule, adaptez votre pratique à la température. Préférez les versants orientés au nord, nord-ouest et nord-est, montez en altitude, partez plus tôt et réduisez l’intensité de votre effort. Buvez davantage, rafraîchissez-vous régulièrement. En cas de survenue de céphalées, nausées, vertiges après un effort en période chaude, mettez-vous au frais, hydratez-vous, rafraîchissez-vous et demandez un avis médical aux secours (112).</li>
                        <li>En forêt ne pas allumer de feu dans des endroits non autorisé ou sensible. Ne jetez pas de déchets et respectez les autres usagers de la montagne.</li>
                    </ul>
                </div>
            </div> 
        </div>

        <div class="safety__item">
            <h2 class="safety__item-title title title--medium">Le balisage et les signaux</h2>
            <div class="safety__item-component" x-data="{ open: false }">
                <span class="safety__item-btn btn btn--purple" @click="open = true">Lire les conseils</span>
                <div class="safety__item-dropdown" x-show="open" @click.away="open = false">
                    <p>Pendant votre sortie, si vous suivez des chemins balisés, vous serez amenés à voir de la signalisation, en voici la signification:</p>
                    <img src="../assets/image/safety/balisage.jpg" alt="Signalisation" class="safety__item-picture">
                    <p>Les signaux de détresse en montagne:</p>
                    <img src="../assets/image/safety/signaux.png" alt="Signaux de détresse" class="safety__item-picture">
                </div>
            </div> 
        </div>

        <div class="safety__item">
            <h2 class="safety__item-title title title--medium">Participer</h2>
            <div class="safety__item-component" x-data="{ open: false }">
                <span class="safety__item-btn btn btn--purple" @click="open = true">Lire les conseils</span>
                <div class="safety__item-dropdown" x-show="open" @click.away="open = false">
                    <p>Vous pouvez si vous le souhaitez, signalez les problèmes rencontrés sur votre itinéraire à travers le dispositif Suricate.</p>
                    <p>Le dispositif Suricate vous permet de signaler les problèmes que vous rencontrez lors de vos sorties :</p>
                    <ul class="safety__item-list">
                        <li>Une erreur de balisage</li>
                        <li>Un panneau défectueux</li>
                        <li>Un problème de pollution</li>
                        <li>Un besoin de sécurisation</li>
                    </ul>
                    <p>Remplissez un formulaire sur l’application Suricate ou sur <strong><a href="http://sentinelles.sportsdenature.fr/">le site Suricate</a></strong>.</p>
                    <p>Votre signalement sera traité par des fédérations sportives de nature, des Départements et des services de l’État en lien avec le Pôle ressource national des sports de nature du ministère chargé des sports. Vous serez tenu informé(e) des suites données à votre signalement.</p>
                </div>
            </div> 
        </div>
    </div>
</section>

<?php get_footer('public'); ?>